import os
import setuptools as setup
from pkg_resources import get_distribution, DistributionNotFound


def find_packages():
    return ['ct'] + ['ct.'+p for p in setup.find_packages('ct')]

requirements = [
    'numpy>=1.19',
    'opencv-python>=4.4',
    'pandas>=1.0',
    'matplotlib',
    'ipywidgets',
    'tqdm',
]

setup.setup(
    name='ct',
    version='1.2.2',
    author='EAVISE',
    description='Basic code template for the labs of compressietechnieken',
    packages=find_packages(),
    install_requires=requirements,
)
