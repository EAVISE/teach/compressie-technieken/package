import numpy as np
from .ctbase import CTBase

__all__ = ['CTStat']


class CTStat(CTBase):
    """
    This class contains 3 extra functions to compute commonly used statistics for image compression.
    See :class:`CTBase` for more information on the proper usage of these classes.
    """
    def compression(self, name, input_img, output_img, input_bpp=8, output_bpp=8):
        """
        Computes Cr & Rd between 2 images.

        Args:
            name (str): Name of the image for which you compute the statistics (used in :func:`CTBase.stat`).
            input_img (np.ndarray): Original image.
            output_img (np.ndarray): Compressed image.
            input_bpp (int, optional): Bits per pixel for the input image. [default: 8]
            output_bpp (int, optional): Bits per pixel for the output image. [default: 8]

        Returns:
            tuple<float,float>: Cr & Rd
        """
        cr = (input_img.size * input_bpp) / (output_img.size * output_bpp)
        rd = 1 - (1 / cr)

        self.stat(f'Cr_{name}', cr)
        self.stat(f'Rd_{name}', rd)
        return cr, rd

    def erms(self, name, input_img, output_img):
        """
        Compute the Erms between 2 images.

        Args:
            name (str): Name of the image for which you compute the statistic (used in :func:`CTBase.stat`).
            input_img (np.ndarray): Original image.
            output_img (np.ndarray): Compressed image.

        Returns:
            float: Erms
        """    
        erms = np.sqrt(np.mean(np.square(output_img - input_img)))
        
        self.stat(f'Erms_{name}', erms)
        return erms

    def entropy(self, name, img):
        """
        Compute the entropy of an image.

        Args:
            name (str): Name of the image for which you compute the statistic (used in :func:`CTBase.stat`).
            img (np.ndarray): image for which you want to compute the entropy.

        Returns:
            float: entropy
        """
        _, freq = np.unique(img, return_counts=True)
        freq = freq / img.size
        entropy = - np.sum(freq * np.log2(freq))
        
        self.stat(f'Entropy_{name}', entropy)
        return entropy