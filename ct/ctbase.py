import os
import random
from math import ceil
import cv2
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
from IPython.display import display
from tqdm.auto import tqdm

__all__ = ['setup_image_folder', 'CTBase']
IMAGE_FOLDER = 'images/'


def setup_image_folder(path):
    global IMAGE_FOLDER
    IMAGE_FOLDER = path


class CTBase:
    """
    This class contains some basic functionality to run compression methods on various images and compute statistics about the methods.

    In order to use this class, one should implement the following methods:
        - `__init__(self, **kwargs)`:
            Initialize your method and store any necessary parameter here.
        - `run(self, img)`:
            This method receives an image, which you should compress with your technique(s).
            The image has already been converted to a grayscale, 32-bit int.

    In the `run()` method, you can use the following convenience functions:
        - `self.imshow(name, img)`:
            Use this method to show an image.
            The name that you passed gets used as the title of the image plot.
        - `self.stat(name, value)`:
            Use this method to show statistics about your method(s).
            This method will accumulate all the statistics and show them nicely formatted in a table.
            When used with `multi_run` (see below), it will compute average statistics automatically.

    To run your models on images, you should first run the `setup_image_folders` function to designate a folder with images on the device.
    Afterwards, you can use the following methods to run your code:
        - `CTBase.single_run(img=None, **kwargs)`:
            This method will run your code on a single image, so that you can develop and debug your code.
            The various `key=value` arguments you pass, are simply passed on to the initialization method.
            You can also pass a number as the image, which will enable you to run your code on a specific image. Otherwise, it is run on a random image from the folder.
        - `CTBase.multi_run(count=30, show_images=False, **kwargs)`:
            This method will run your code on a plethora of different images and should be used to compute meaningfull statistics about your compression techniques.
            The various `key=value` arguments you pass, are simply passed on to the initialization method.
            You can also decide how many images should be processed and whether are not to show the images you plotted using the `imshow()` method described above.
    """
    @classmethod
    def single_run(cls, img=None, **kwargs):
        """
        Run you code on a single image.

        Args:
            img (int, optional): The index of the image you want to use. [Default: random image]
            **kwargs (optional): Arguments that are passed to the `__init__()` method
        """
        instance = cls(**kwargs)
        instance._setup()

        img = cv2.imread(instance._get_random_images(selection=[img] if img is not None else None)[0], cv2.IMREAD_GRAYSCALE).astype(np.int32)
        instance.run(img)

        instance._print()

    @classmethod
    def multi_run(cls, count=30, show_images=False, **kwargs):
        """
        Run you code on a variety of different images.

        Args:
            count (int, optional): The number of images that are processed. [Default: 30]
            show_images (bool, optional): Whether to show the images that are plotted with :func:`CTBase.imshow`.
            **kwargs (optional): Arguments that are passed to the :func:`CTBase.__init__` method
        """
        instance = cls(**kwargs)
        instance._setup()

        img_names = instance._get_random_images(count)
        for img in tqdm(img_names, desc="Images"):
            img = cv2.imread(img, cv2.IMREAD_GRAYSCALE).astype(np.int32)
            instance.run(img)

        instance._print(show_images)

    def imshow(self, name, image):
        """
        Plot images for visual inspection.

        Args:
            name (str): Name that is printed above the image and is also used for grouping with :func:`CTBase.multi_run`.
            image (np.ndarray): The image you want to show.

        Note:
            By default images are not shown when using :func:`CTBase.multi_run`, but you can enable it by setting the `show_images` argument to **True**.
        """
        if name in self.__images:
            self.__images[name].append(image)
        else:
            self.__images[name] = [image]

    def stat(self, name, value):
        """
        Show a certain statistic.

        Args:
            name (str): Name of the statistic & image pair for which you compute the statistic (used for grouping with :func:`CTBase.multi_run`).
            value (number): The value of the statistic.

        Note:
            The name for your statistic should be unique for each statistic and each different image you use it on.
            This allows the class to compute averages of each `statistics, image` pair, allowing you to make toughtfull conclusions about your techniques.
        """
        if name in self.__stats:
            self.__stats[name].append(value)
        else:
            self.__stats[name] = [value]

    def _setup(self):
        self.__images = dict()
        self.__stats = dict()

    def _get_random_images(self, count=1, selection=None):
        global IMAGE_FOLDER

        choices = sorted([os.path.join(IMAGE_FOLDER, filename) for filename in os.listdir(IMAGE_FOLDER) if os.path.isfile(os.path.join(IMAGE_FOLDER, filename))])
        if selection is not None:
            if max(selection) >= len(choices):
                raise ValueError(f'Selection index out of bounds! [images: {len(choices)} | selection: {max(selection)}]')
            return [choices[i] for i in selection]
        else:
            if count > len(choices):
                raise ValueError(f'There are not enough images available! [images: {len(choices)} | selected: {count}]')
            return random.sample(choices, count)

    def _print(self, show_images=True):
        # Statistics
        if len(self.__stats) > 0:
            self.__stats = pd.DataFrame(self.__stats)
            if len(self.__stats) > 1:
                self.__stats.loc[-1] = self.__stats.mean()
                self.__stats = self.__stats.sort_index()
                self.__stats.rename({-1: 'Average'}, inplace=True)
            with pd.option_context('display.max_rows', None, 'display.max_columns', None, 'display.width', None):
                display(self.__stats)

        # Images
        if show_images and len(self.__images) > 0:
            # Option 1: single run
            if all(len(v) == 1 for v in self.__images.values()):
                cols = min(len(self.__images), 5)
                rows = ceil(len(self.__images) / 5)

                fig = plt.figure(dpi=600)
                grid = ImageGrid(fig, 111, nrows_ncols=(rows, cols), axes_pad=(0.1, 0.25))

                for i, (name, img) in enumerate(self.__images.items()):
                    img = img[0]

                    grid[i].set_yticks([])
                    grid[i].set_xticks([])
                    grid[i].imshow(img.astype(np.uint8), cmap='gray')
                    grid[i].set_title(name, fontsize=6)

                for i in range(i+1, len(grid)):
                    fig.delaxes(grid[i])

                plt.show()

            # Option 2: multi run
            else:
                for name, images in self.__images.items():
                    cols = min(len(images), 5)
                    rows = ceil(len(images) / 5)

                    fig, axes = plt.subplots(rows, cols, squeeze=False, dpi=600)
                    axes = axes.flatten()

                    for i, img in enumerate(images):
                        axes[i].set_yticks([])
                        axes[i].set_xticks([])
                        axes[i].set_anchor('NW')
                        axes[i].imshow(img.astype(np.uint8), cmap='gray')
                        if i == 0:
                            axes[i].set_title(name, fontsize=6)
                    
                    for i in range(i+1, len(axes)):
                        fig.delaxes(axes[i])

                    plt.tight_layout()
                    plt.show()

        # Reset
        self._setup()
